/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotelmanagment;

import static com.sun.org.apache.xalan.internal.lib.ExsltDatetime.date;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Dulan
 */
public class reservation {
    
    // 2 foreign keys; one for client, one for room
    //for client
    //alter TABLE reservations add CONSTRAINT fk_client_id FOREIGN key (client_id) REFERENCES clients(id) on DELETE CASCADE
    //for room
    //alter TABLE reservations add CONSTRAINT fk_room_number FOREIGN key (room_number) REFERENCES rooms(r_number) on DELETE CASCADE
    
    // another fk between table types and rooms
    //alter TABLE rooms add CONSTRAINT fk_type_number FOREIGN key (type) REFERENCES type(id) on DELETE CASCADE
    
    MyConnection my_connection = new MyConnection();
    
    public boolean addReservation(int client_id, int room_number, String DateIn, String DateOut){
        //INSERT INTO `reservations`(`client_id`, `room_number`, `date_in`, `date_out`) VALUES (?,?,?,?)
        PreparedStatement st;
        String addQuery = "INSERT INTO `reservations`(`client_id`, `room_number`, `date_in`, `date_out`) VALUES (?,?,?,?)";
        
        try {
            st = my_connection.createConnection().prepareStatement(addQuery);
            
            st.setInt(1, client_id);
            st.setInt(2, room_number);
            st.setString(3, DateIn);
            st.setString(4, DateOut);
            
            return (st.executeUpdate() > 0);
            
            
        } catch (SQLException ex) {
            Logger.getLogger(Client.class.getName ()).log(Level.SEVERE, null, ex);
            return false;
            
        }
    }
            
    public boolean editReservation(int reservation_id, int client_id, int room_number, String date_in, String date_out){
        
        PreparedStatement st;
        String editQuery = "UPDATE `reservations` SET `client_id`=?,`room_number`=?,`date_in`=?,`date_out`=? WHERE `id` =?";
        
        try{
            st = my_connection.createConnection().prepareStatement(editQuery);
            
            st.setInt(1, client_id);
            st.setInt(2, room_number);
            st.setString(3, date_in);
            st.setString(4, date_out);
            st.setInt(5, reservation_id);
            
            return (st.executeUpdate() > 0);
        }
        catch (SQLException ex) {
            Logger.getLogger(Client.class.getName ()).log(Level.SEVERE, null, ex);
            return false;
          }
    }
    
    //removing a selected reservation
    public boolean removeReservation(int reservation_id){
        PreparedStatement st;
        String deleteQuery = "DELETE FROM `reservations` WHERE `id`=?";
        
        try{
            st = my_connection.createConnection().prepareStatement(deleteQuery);
            
            st.setInt(1, reservation_id);
            return (st.executeUpdate() > 0);
        }
        catch (SQLException ex) {
            Logger.getLogger(Client.class.getName ()).log(Level.SEVERE, null, ex);
            return false;
          }
    }
    
    //a function to display stuff in the table
    public void fillReservationsJTable(JTable table){
        
        PreparedStatement ps;
        ResultSet rs;
        String selectQuery = "SELECT * FROM `reservations`";
        
        try{
            
            ps = my_connection.createConnection().prepareStatement(selectQuery);
            rs = ps.executeQuery();
            
            DefaultTableModel tableModel = (DefaultTableModel)table.getModel();
            
            Object [] row;
            
            while(rs.next()){
                row = new Object[5];
                row[0] = rs.getInt(1);
                row[1] = rs.getInt(2);
                row[2] = rs.getInt(3);
                row[3] = rs.getString(4);
                row[4] = rs.getString(5);
                
                tableModel.addRow(row);
            }
        }
        catch (SQLException ex) {
            Logger.getLogger(Client.class.getName ()).log(Level.SEVERE, null, ex);
          }
        
    }
    
}